/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
extern uint8 const CYCODE LCD_Khanas_customFonts[];
int main(void)
{
    uint8 pos = 1u;
    uint8 row = 0u;
    
    LCD_Khanas_Start();
    LCD_Khanas_LoadCustomFonts(LCD_Khanas_customFonts);
    
    LCD_Khanas_Position(row, pos);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_0);
    pos += 2u;
    
    LCD_Khanas_Position(row, pos);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_1);
    pos += 2u;
    
    LCD_Khanas_Position(row, pos);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_2);
    pos += 2u;
    
    LCD_Khanas_Position(row, pos);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_3);
    pos += 2u;
    
    LCD_Khanas_Position(row, pos);
    LCD_Khanas_PutChar(LCD_Khanas_CUSTOM_4);
    pos += 2u;
    
    CyDelay(200u);
    LCD_Khanas_Position(1u, 9u);
    LCD_Khanas_PrintString("VASYL");
    
    //CyGlobalIntEnable; /* Enable global interrupts. */
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
